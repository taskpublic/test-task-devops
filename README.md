## Summary

Its a test task for Junior DevOps vacancy.

## Author

- [ ] [Author](t.me/ucishka2)  in Telegram

## Tasks completed within the assignment

-  Python module written
-  The python model is compiled into an rpm package
-  The python model is compiled into an deb package
-  Configured to build the package in a docker container
-  Configured to automatically build the package by commit in gitlab ci/cd
-  Added dependency python3>= 3.6
-  Implemented adding the default.conf file when building along the path /etc/hello_world/default.conf

