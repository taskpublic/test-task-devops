#!/bin/bash

echo 'debconf debconf/frontend select Noninteractive' |  debconf-set-selections
DEBIAN_FRONTEND=noninteractive  apt-get update &&  apt-get -y --no-install-recommends install tzdata
cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime
apt-get update && apt-get install -y tzdata gzip fakeroot python-all python3 dh-python python3-stdeb python3-pip ruby build-essential rubygems ruby rpm dpkg-dev
pip install gdown

gdown https://drive.google.com/uc?id=1Wdunt3ajFFgafsl48MasbxGBTheUkYzw
#gdown https://drive.google.com/uc?id=1naFn0FI4nPMQlD1lpyWOATORBRVkXcYi
tar -xzf hello_world.tar.gz && rm hello_world.tar.gz
ls -la

pkg=deb
if [ $pkg==rpm ]; then
    echo "pkg set to rpm"
    sed -i -e 's/pkg=deb/pkg=rpm/' default.conf
	python3 setup.py bdist_rpm
    

elif [ $pkg==deb ]; then
    echo "pkg set to deb"
	sed -i -e 's/pkg=rpm/pkg=deb/' default.conf 
	
else 
echo "No parametrs"
fi

python3 setup.py --command-packages=stdeb.command install_deb
#chmod +x build_package.sh
#./build_package.sh deb
#python3 setup.py --command-packages=stdeb.command install_deb


